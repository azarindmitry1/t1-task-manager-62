package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    TaskDto updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}