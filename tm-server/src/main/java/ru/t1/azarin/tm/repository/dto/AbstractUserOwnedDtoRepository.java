package ru.t1.azarin.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedDtoModel;

@Repository
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends AbstractDtoRepository<M> {

}
