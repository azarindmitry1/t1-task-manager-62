package ru.t1.azarin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeStatusByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectClearResponse clearResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    );

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectCreateResponse createResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    );

    @NotNull
    @WebMethod
    ProjectListResponse listResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    );

}
